#集貼紙抽獎
'''
五張貼紙可以換一張抽獎券
抽獎有三成的機會抽到一隻熊玩偶&兩張貼紙
抽獎有七成的機會抽到三張貼紙
要將目前的狀態回報給使用者，
包括：有幾張抽獎券、每一次的抽獎結果、剩下幾張貼紙。
在最後輸出贏得的玩偶數、最後剩下的貼紙。
'''
import random as rd
odds = rd.random()
sticker = eval(input("Please enter your sticker amount: "))
bear = 0
while(sticker >= 5):
    
    ticket = sticker // 5
    sticker = sticker - ticket * 5
    print(f"You have {ticket} ticket! Good luck!")
    
    while(ticket > 0):
        ticket -= 1
        
        if (odds <= 0.3):
            print("You win a bear and 2 stickers!")
            bear += 1
            sticker += 2 
        else:
            print("You win 3 stickers!")
            sticker += 3
            
    
    print(f"You still have {sticker} stickers retaining!")
    ticket = sticker // 5
    
print("-------------------------------------------------")   
print(f"You win {bear} bears and still have {sticker} sticker left !")