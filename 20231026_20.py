IC = eval(input("輸入全年綜合所得："))

#假設2023年的個人免稅額為30萬元
NIC = IC - 300000

if (NIC<1):
    income_tax = 0
elif (NIC<540001):
    income_tax = NIC * 0.05
elif (NIC<1210001):
    income_tax = NIC * 0.12
elif (NIC<2420001):
    income_tax = NIC * 0.20
elif (NIC<4530001):
    income_tax = NIC * 0.30
else:
    income_tax = NIC * 0.40 

print("今年的所得稅為：", income_tax)
print("月平均可支用所得為：", (IC - income_tax) / 12)
