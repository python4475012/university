#混和溶液
#要將濃度45%的食鹽水x公克和濃度75%的食鹽水y公克，混合成p%的食鹽水n公克。請設計一支程式，讓使用者輸入(p)濃度、(n)克，來算出合理的x、y。
n = eval(input("Please enter n(g):")) 
p = eval(input("Please enter p(%):")) 
if p < 45 or p > 75:
    print("False")
else:
    #濃度45%體積1+濃度75%體積2=混合溶液濃度p體積3。即: M1xV1+M2xV2=M*V
    #都是食鹽水，不用換單位
    #45*x+75*y=p*n
    x = (n * (75 - p)) / 30
    y = n - x
print(f'45%: {x}g, 75%: {y}g')