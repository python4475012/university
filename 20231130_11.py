'''
建造金字塔
從使用者取得金字塔的高度‧如果是3層的話，列印出以下的圖案：
  *       空空星
 ***      空星星星
*****     星星星星星
'''
def PrintS(num,pS):
    print(pS*num,end='')
    
N = int(input())

for i in range(1,N+1):  #i = 1,2,3...N
    # i : 1,2,3...
    #Star = 1,3,5
    #Spac: N-1,N-2,N-3...0 
    NoS = 2 * i - 1
    NoSp = N - i
    PrintS(NoSp," ")#印NoSp個空白，不換行
    PrintS(NoS,'*')
    print('')
    