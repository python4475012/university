N = int(input())
CT = 0
for x in range(2,N+1):
    flag = True #True = Prime number, False = Not a prime number

    for i in range(2, int(x ** 0.5) + 1):
        if x % i == 0:
            flag = False
            break

    if flag:
        print(x, end=' ')
        CT += 1
        if CT % 10 == 0:
            print('')