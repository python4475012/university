#分解硬幣組合
'''
給定一正整數表示零錢的總金額，
請回答此金額有多少硬幣組合。
例如：輸入765代表金額是765元，
輸出34096代表此金額共有34096種硬幣組合。
'''
money = int(input('Please enter monet: '))
Max50 = money//50
count = 0   #設count為記數器，
#Max50為總金額最多可以除幾個五十塊。


for i in range(Max50+1):
    aValue = money - i*50
    Max10 = aValue//10
    
    for j in range(Max10+1):
        bValue = money - i*50 - j*50
        Max5 = bValue//5
        count = count + Max5 +1
'''
使用for迴圈計算排列組合，
必須計算1個50、2個50以此類推等情況，
故設range(Max50+1)。       
最後計算在各種情況下Max5+1的組合。
1為全部都是一塊錢的情況。
'''
(f'result:{count}')