#雞兔同籠
#籠子裡面只有放兔子和雞兩種動物，請撰寫一程式輸入籠子裡的動物總數(n)、腳的總數(f)，並計算出符合的兔子數量(r)和雞的數量(c)，若是計算有誤則輸出False。
animal_amount = eval(input("Please enter animal amount:"))
feet_amount = eval(input("Please enter feet amount:"))

if feet_amount % 2 == 1 or animal_amount * 2 > feet_amount or animal_amount * 4 < feet_amount :
    print('False')
else:
    r = (feet_amount - (2 *animal_amount)) // 2
    c = animal_amount - r
    print(r, c) 