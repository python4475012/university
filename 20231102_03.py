#W8 Exercise 6-0
import random as rd

N = int(input('Please input N:'))
idx = 1
Cth = 0

while (idx <= N):
    Cth += rd.randint(0,1)  #Head 1, Tail 0
    idx  += 1

print("# Head = {:>5d}({:^7.1%})".format(Cth, Cth/N))