#質數分數相乘
'''
任意輸入一職數上限值m，
相乘1/2、1/3、1/5 ... 1/m並格式化輸出，
小數點三位後四捨五入
'''
def is_prime(x):
    if x == 2:
        return True
    elif x % 2 == 0:
        return False
    else:
        for i in range(3,int(x**0.5)+1,2):
            if x % i == 0:
                return False
    return True

m = int(input('Please enter number: ')) #因題意為每個質數倒數相乘，故設分母num為一。
num = 1
for i in range(2,m+1):
    if is_prime(i) == True:
        num = num/i
'''
or迴圈設定i的範圍(2~m)，
並利用is_prime()自訂函式判斷i是否為質數，
若為True則將num/i。
'''
print(f'result:{num:.3f}')