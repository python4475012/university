#簡單的計算器
number1 = eval(input("輸入第一個數字:"))
mathematical = (input("輸入運算符(+, -, *, /):"))
number2 = eval(input("輸入第二個數字:"))
if (mathematical == "+"):
    result = number1 + number2
elif (mathematical == "-"):
    result = number1 - number2
elif (mathematical == "*"):
    result = number1 * number2
elif (mathematical == "/"):
    if(number2 == 0):
        print("除數不得為零")
    else:
        result = number1 / number2
    
print("結果:", result)