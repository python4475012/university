#W8 Exercise 6-1
key = 3
CtF = 0
CtM = 0
while (key != 0):
    key = int(input("Input 1 for Female, 2 Male, 0 to quit:"))
    
    if (key == 1):
        CtF += 1
    if (key == 0):
        CtM += 1

print(f"Female:{CtF}, Male:{CtM}")