def GCD(x,y):
    RV = 1
    
    idx = 2
    while (idx<=x and idx<=y):
        if(x%idx == 0) and (y%idx == 0):
            RV = idx
        idx+=1
        
    return RV

A = int(input())
B = int(input())
GCD = GCD(A,B)
print(f'GCD({A},{B})={GCD}')
print(f'LCM({A},{B})={int(A*B/GCD)}')
##P.S:  A*B = GCD(A,B) * LCM(A,B)