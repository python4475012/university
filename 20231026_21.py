a = eval(input("請輸入a參數:"))
b = eval(input("請輸入b參數:"))
c = eval(input("請輸入c參數:"))

D = b ** 2 - 4 * a * c

if (D < 0):
    print("x沒有實根")
elif (D == 0):
    print("x為重根：")
    print("x為", -b / 2 * a)
else:
    print("x有二實根：")
    print("x1為", (-b - D ** 0.5) / 2 * a, ", x2為",(-b + D ** 0.5) / 2 * a )
