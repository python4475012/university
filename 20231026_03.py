#sin cos tan
import math as mt

da = eval(input('get degree a:'))
ra = mt.radians(da)
sin_a = mt.sin(ra)
cos_a = mt.cos(ra)
tan_a = mt.tan(ra)

print('sin(a)=', sin_a)
print('cos(a)=', cos_a)
print('tan(a)=', tan_a)