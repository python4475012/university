#找質數
x = int(input())

flag = True #True = Prime number, False = Not a prime number

for i in range(2, int(x ** 0.5) + 1):
    if x % i ==0:
        flag = False
        break

if flag:
    print(f'{x} is a prime number.')
else:
    print(f'{x} is not a prime number.')

