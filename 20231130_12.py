#From Isprime0.py
'''
列出小於N的質數 
撰寫一個檢查是否是質數的函式IsP
質數回答True
非質數回答False
從使用者取得一數N
從2到N一一使用IsP判斷是否為質數
True 列出
'''
def IsP(x):
    flag = True #True = Prime number, False = Not a prime number

    for i in range(2, int(x ** 0.5) + 1):
        if x % i ==0:
            flag = False
            break
    
    return flag
N = int(input())

for i in range(2,N):
   if IsP(i):
       print(i)