import random as rd

idx = 0
ct1 = 0 
ct2 = 0
ct3 = 0
ct4 = 0
ct5 = 0
print("#Player:     ", end="")
while (idx<5):
    seed = rd.randint(1,6)
    print(seed, end=" ")
    if seed == 1:
        ct1 += 1
    if seed == 2:
        ct2 += 1
    if seed == 3:
        ct3 += 1
    if seed == 4:
        ct4 += 1
    if seed == 5:
        ct5 += 1
    idx += 1
print("Score:", ct1 + ct2 + ct3 + ct4 + ct5)

idx = 0
ct1 = 0 
ct2 = 0
ct3 = 0
ct4 = 0
ct5 = 0
print("#Computer:     ", end="")
while (idx<5):
    seed2 = rd.randint(1,6)
    print(seed2, end=" ")
    if seed2 == 1:
        ct1 += 1
    if seed2 == 2:
        ct2 += 1
    if seed2 == 3:
        ct3 += 1
    if seed2 == 4:
        ct4 += 1
    if seed2 == 5:
        ct5 += 1
    idx += 1
print("Score:", ct1 + ct2 + ct3 + ct4 + ct5)

if seed2 > seed :
    print("Computer Win!")
else:
    print("Player Win!")