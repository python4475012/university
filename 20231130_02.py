#判斷是否為質數
def is_prime(x):
    if x == 2:
        return True
    elif x % 2 == 0:
        return False
    else:
        for i in range(3,int(x**0.5)+1,2):
            if x % i == 0:
                return False
    return True
'''
利用for迴圈來做一一檢查，
range()範圍由3開始，終點則是根號x+1，
間隔設為2(因只需看奇數，偶數已經排除)。
x需除每一次的i來檢查是否為質數。
若有整除則回傳False，
反則跳出迴圈並回傳True。
'''
a = int(input('Please enter number: '))
print(is_prime(a))